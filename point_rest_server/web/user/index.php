<!DOCTYPE html>
<html lang="en">
<head>
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <script src="assets/scripts/jquery.min.js"></script>
  <script src="assets/scripts/bootstrap.min.js"></script>
  <script src="assets/scripts/sweetalert28.js"></script>
  <script>
    var userJSON;
    function clear(classtarget) {
        $("."+classtarget).empty();
    }
    function queryuser() {
        $.get("http://35.247.161.218:3000/checkPoint?userID=114", function(data, status){
            userJSON = JSON.parse(data);
            $(".container").html("<h1>ยินดีต้อนรับคุณ "+userJSON.name+"</h1>");
            $(".container").append("<h2>ยอดแต้มสะสม "+userJSON.point+" แต้ม</h2>");
        });
        
    }
    function showredeem() {
        $(".container").html("<form><fieldset class='form-group'><legend>แลกแต้ม</legend><label>รหัสร้านค้า</label><input class='form-control' id='shopID' type='text'><label>จำนวนแต้มที่จะแลก</label><input class='form-control' id='point' type='number' min='0' max='1000000'><button id='redeemBT' class='form-control btn btn-success'>แลก</button></fieldset></form>");
        
    }
    function submitredeem(shopid,userid,point) {
        if (userJSON.point>=point) {
            $.get("http://35.247.161.218:3000/redeemPoint?shopID="+shopid+"&userID="+userid+"&point="+point, function(data, status){
                if(JSON.parse(data).result=="200"){
                    console.log(JSON.parse(data).result);
                    Swal.fire(
                        'Good job!',
                        'You clicked the button!',
                        'success'
                    )
                }
                else{
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Errorrrrrrrr'
                    })
                }
                
                document.getElementById("redeemBT").disabled = false;
            });
        }
        else{
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Not enough balance'
            })
        }
        
    }
    $(document).ready(function() {
        $("#home").addClass("active");
        queryuser();
        $("#home").click(function(){
            $("#home").addClass("active");
            $("#pay,#redeem").removeClass("active");
            clear("container");
            $(".container").append("<div class='lds-dual-ring'></div>");
            queryuser();
        });
        $("#pay").click(function(){
            $("#pay").addClass("active");
            $("#home,#redeem").removeClass("active");
            clear("container");
        });
        $("#redeem").click(function(){
            $("#redeem").addClass("active");
            $("#home,#pay").removeClass("active");
            clear("container");
            showredeem();
            $("#redeemBT").click(function(){
                document.getElementById("redeemBT").disabled = true;
                var shopid = document.getElementById("shopID").value;
                var point = document.getElementById("point").value;
                submitredeem(shopid,"114",point);
            });
        });
    });
  </script>
  <style>
    #home{
        width: 30%;
    }
    #pay{
        width: 30%;
    }
    #redeem{
        width: 40%;
    }
    .lds-dual-ring {
        display: inline-block;
        width: 64px;
        height: 64px;
        align-content:center;
    }
    .lds-dual-ring:after {
        content: "  ";
        display: block;
        width: 46px;
        height: 46px;
        margin: 1px;
        border-radius: 50%;
        border: 5px solid #999;
        border-color: #999 transparent #999 transparent;
        animation: lds-dual-ring 1.2s linear infinite;
    }
    @keyframes lds-dual-ring {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

  </style>
</head>
<body>
    <nav class="navbar">
        <div class="container-fluid">
            <ul class="nav nav-tabs">
                <li id="home"><a>หน้าหลัก</a></li>
                <li id="pay"><a>ชำระเงิน</a></li>
                <li id="redeem"><a>แลกแต้มสะสม</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <div class="lds-dual-ring"></div>
    </div>
</body>
</html>