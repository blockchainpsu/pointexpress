const express = require('express')
const app = express()
const port = 3000


// Contract Function
'use strict';

const { FileSystemWallet, Gateway } = require('fabric-network');
const {} = require('arraybuffer-to-string');
const fs = require('fs');
const path = require('path');

async function checkUserPoint(userID) {
  try {

    const ccpPath = path.resolve(__dirname, 'connection.json');
    const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));
    const walletPath = path.resolve(__dirname, 'wallet');
    const wallet = new FileSystemWallet(walletPath);
    
    const gateway = new Gateway();
    await gateway.connect(ccp, { wallet, identity: 'chinna1' , discovery: {enabled: true, asLocalhost:false }});

    const network = await gateway.getNetwork('channel1');

    const contract = network.getContract('point');
    const result = await contract.evaluateTransaction('checkUserPoint', userID);
    
    console.log(Buffer.from(result).toString('utf8'));
    await gateway.disconnect();
	return Buffer.from(result).toString('utf8');

    } catch (error) {
      console.error(`Failed to submit transaction: ${error}`);
	return 0;
    }
}


async function givePoint(userID, point) {
  try {

    const ccpPath = path.resolve(__dirname, 'connection.json');
    const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));
    const walletPath = path.resolve(__dirname, 'wallet');
    const wallet = new FileSystemWallet(walletPath);
    
    const gateway = new Gateway();
    await gateway.connect(ccp, { wallet, identity: 'chinna1' , discovery: {enabled: true, asLocalhost:false }});

    const network = await gateway.getNetwork('channel1');

    const contract = network.getContract('point');
    const result = await contract.submitTransaction('givePoint', userID, point);
    
    await gateway.disconnect();
    var result2 = {
	"result" : 200
    }
    return JSON.stringify(result2);

    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
	return `Failed to submit transaction: ${error}`;
    }
}


async function createShop(shopID, shopName) {
  try {

    const ccpPath = path.resolve(__dirname, 'connection.json');
    const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));
    const walletPath = path.resolve(__dirname, 'wallet');
    const wallet = new FileSystemWallet(walletPath);
    
    const gateway = new Gateway();
    await gateway.connect(ccp, { wallet, identity: 'chinna1' , discovery: {enabled: true, asLocalhost:false }});

    const network = await gateway.getNetwork('channel1');

    const contract = network.getContract('point');
    const result = await contract.submitTransaction('createShop', shopID, shopName,0);
    
    await gateway.disconnect();
    var result2 = {
	"result" : 200
    }
    return JSON.stringify(result2);

    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
	return `Failed to submit transaction: ${error}`;
    }
}




async function redeemPoint(shopID, userID, point) {
   
  try {

    const ccpPath = path.resolve(__dirname, 'connection.json');
    const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));
    const walletPath = path.resolve(__dirname, 'wallet');
    const wallet = new FileSystemWallet(walletPath);
    
    const gateway = new Gateway();
    await gateway.connect(ccp, { wallet, identity: 'chinna1' , discovery: {enabled: true, asLocalhost:false }});

    const network = await gateway.getNetwork('channel1');

    const contract = network.getContract('point');

 
    const result = await contract.submitTransaction('redeemPoint', shopID, userID, point);
    
    await gateway.disconnect();
    var result2 = {
	"result" : 200
    }
    return JSON.stringify(result2);

    } catch (error) {
	 var result2 = {
		"result" : "Error, Not enough balance to redeem "+point+" points "
    	}
        console.error(`Failed to submit transaction: ${error}`);
	return JSON.stringify(result2);
    
    }
}


app.get('/checkPoint', (req, res) => {
				test = checkUserPoint(req.query["userID"]).then( (result) => {  res.send(result); } ).
							catch(function (error) {
  								console.error(error)
						           });
			   }
		);

app.get('/givePoint', (req, res) => {
				
				test = givePoint(req.query["userID"], 
						 req.query["point"]).then( (result) => {  res.send(result); } ).
							catch(function (error) {
  								console.error(error)
						           });
			   }
		);


app.get('/createShop', (req, res) => {
				
				test = createShop(req.query["shopID"], 
						 req.query["shopName"]).then( (result) => {  res.send(result); } ).
							catch(function (error) {
  								console.error(error)
						           });
			   }
		);


app.get('/redeemPoint', (req, res) => {
				
				test = redeemPoint(req.query["shopID"], 
						 req.query["userID"],
						 req.query["point"]).then( (result) => {  res.send(result); } ).
							catch(function (error) {
  								console.error(error)
						           });
			   }
		);


app.listen(port, () => console.log(`Example app listening on port ${port}!`))
